package com.shelpuksoftcorp.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Rectangle extends Figure{
    private double halfHeight;
    private double halfWidth;

    public Rectangle(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_RECTANGLE, cx, cy, lineWidth, color);
    }

    public Rectangle(double cx, double cy, double lineWidth, Color color, double halfHeight, double halfWidth) {
        this(cx, cy, lineWidth, color);
        this.halfHeight = halfHeight < 10 ? 10 : halfHeight;
        this.halfWidth = halfWidth < 10 ? 10 : halfWidth;
    }

    public double getHalfHeight() {
        return halfHeight;
    }

    public double getHalfWidth() {
        return halfWidth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.halfHeight, halfHeight) == 0 &&
                Double.compare(rectangle.halfWidth, halfWidth) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(halfHeight, halfWidth);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "halfHeight=" + halfHeight +
                ", halfWidth=" + halfWidth +
                '}';
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeRect(cx - halfWidth, cy - halfHeight, halfWidth * 2, halfHeight * 2);
    }
}